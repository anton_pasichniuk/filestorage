import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalConstants } from 'src/app/constanst';
import { AuthService } from 'src/app/services/auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  error = "";

  constructor(
      private formBuilder: FormBuilder,
      private route: ActivatedRoute,
      private router: Router,
      private authenticationService: AuthService)
  {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  ngOnInit() {
    if(sessionStorage.getItem(GlobalConstants.JWT_NAME)){
      this.router.navigate(['/']);
    }
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  onSubmit() {
      this.submitted = true;

      // stop here if form is invalid
      if (this.loginForm.invalid) {
          return;
      }

      this.loading = true;
      this.authenticationService.login({
        email: this.f.email.value,
        password: this.f.password.value
      })
      .subscribe(result => {
        sessionStorage.setItem(GlobalConstants.JWT_NAME, result.accessToken);
        sessionStorage.setItem(GlobalConstants.USER_NAME, JSON.stringify({
          userId: result.userId,
          username: result.username,
          userRoles: result.userRoles
        }))
        this.router.navigate(["/"])
      }, error => {
        this.loading = false;
        this.error = "Email or password incorrect";
      });
  }
}
