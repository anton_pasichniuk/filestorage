import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UpdateUser } from '../models/UpdateUserModel';
import { User } from '../models/UserModel';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private readonly BASE_URL = "https://localhost:44363/api/users";

  constructor(private http: HttpClient) {}

  getAllUsers(): Observable<User[]>{
    return this.http.get<User[]>(`${this.BASE_URL}`);
  }

  getUserPersonalInfo(): Observable<User> {
    return this.http.get<User>(`${this.BASE_URL}/personalInfo`);
  }

  getUserInfo(userId: string): Observable<User> {
    return this.http.get<User>(`${this.BASE_URL}/personalInfo/${userId}`);
  }

  updateUserInfo(newUserInfo: UpdateUser) {
    return this.http.patch<any>(`${this.BASE_URL}/change`, newUserInfo);
  }

  updateUserInfoAdmin(newUserInfo: UpdateUser, userId: string) {
    return this.http.patch<any>(`${this.BASE_URL}/change/${userId}`, newUserInfo);
  }

  deleteUser() {
    return this.http.delete<any>(`${this.BASE_URL}/delete`);
  }

  deleteUserAdmin(userId: string) {
    return this.http.delete<any>(`${this.BASE_URL}/delete/${userId}`);
  }
}
