import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FileInfo } from '../models/FileModel';
import { FileUser } from '../models/FileUserModel';

@Injectable({
  providedIn: 'root'
})
export class FileService {
  private readonly BASE_URL = "https://localhost:44363/api/files";

  constructor(private http: HttpClient) {}

  getAllFiles(): Observable<FileUser[]> {
    return this.http.get<FileUser[]>(`${this.BASE_URL}`);
  }

  getUserFiles(): Observable<FileInfo[]> {
    return this.http.get<FileInfo[]>(`${this.BASE_URL}/user`);
  }

  getSharedFiles(): Observable<FileUser[]> {
    return this.http.get<FileUser[]>(`${this.BASE_URL}/shared`);
  }

  uploadFile(file: File) {
    return this.http.post<any>(`${this.BASE_URL}/create`, file);
  }

  giveAccess(userName: string, fileId: string) {
    return this.http.post<any>(`${this.BASE_URL}/access/give/${userName}`, fileId);
  }

  removeAccess(userName: string, fileId: string) {
    return this.http.post<any>(`${this.BASE_URL}/access/remove/${userName}`, fileId);
  }

  changePrivacy(fileId: string) {
    return this.http.patch<any>(`${this.BASE_URL}/privacy/${fileId}`, null);
  }

  changeName(fileId: string, fileName: string) {
    return this.http.patch<any>(`${this.BASE_URL}/privacy/${fileId}`, fileName);
  }

  download(fileId: string): Observable<File> {
    return this.http.get<File>(`${this.BASE_URL}/download/${fileId}`);
  }

  delete(fileId: string) {
    return this.http.delete<any>(`${this.BASE_URL}/delete/${fileId}`);
  }
}
