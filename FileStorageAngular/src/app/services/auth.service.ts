import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { GlobalConstants } from '../constanst';
import { Login } from '../models/LoginModel';
import { LoginRequest } from '../models/LoginRequestModel';
import { Register } from '../models/RegisterModel';

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  private readonly BASE_URL = "https://localhost:44363/api/auth";

  constructor(private http: HttpClient) {}

  login(user: Login): Observable<LoginRequest> {
    return this.http.post<LoginRequest>(`${this.BASE_URL}/login`, user);
  }  

  register(newUser: Register) {
    return this.http.post<any>(`${this.BASE_URL}/register`, newUser);
  }

  logout() {
    sessionStorage.removeItem(GlobalConstants.USER_NAME);
    sessionStorage.removeItem(GlobalConstants.JWT_NAME)
  }
}
