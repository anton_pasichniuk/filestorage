export interface FileUser {
    fileId: string;
    name: string;
    size: number;
    version: number;
    createdDate: Date;
    userName: string;
    isPublic: string;
}