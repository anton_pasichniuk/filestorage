export interface LoginRequest {
    accessToken: string;
    userId: string;
    username: string;
    userRoles: string[];
}