export interface FileInfo {
    fileId: string;
    name: string;
    size: number;
    version: number;
    createdDate: Date;
    isPublic: boolean;
}