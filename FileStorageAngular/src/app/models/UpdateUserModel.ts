export interface UpdateUser {
    userName: string;
    phoneNumber: string;
}