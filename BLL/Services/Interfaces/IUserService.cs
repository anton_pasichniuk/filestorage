﻿using BLL.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services.Interfaces
{
    public interface IUserService
    {
        List<UserModel> GetAll();
        Task<UserModel> GetByIdAsync(string id);
        Task AddAsync(UserModel model);
        Task UpdateAsync(string userId, UpdateUserModel newUserInfo);
        Task DeleteByIdAsync(string userId);
        Task<UserModel> FindByEmailAndPasswordAsync(string email, string password);
        Task<IList<string>> GetRolesAsync(string id);
        Task<bool> IsEmailExistsAsync(string email);
    }
}
