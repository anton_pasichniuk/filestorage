﻿using BLL.Models;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace BLL.Services.Interfaces
{
    public interface IFileService
    {
        Task<List<FileUserModel>> GetFilesAsync(string userId = null);
        List<FileModel> GetUserFiles(string userId);
        List<FileUserModel> GetFilesWithAccess(string userId);
        Task AddAsync(IFormFile form, string userId);
        Task DeleteByIdAsync(string fileId, string userId);
        Task GiveAccessAsync(string fileId, string userNameToGiveAccess, string userId);
        Task RemoveAccessAsync(string fileId, string userNameToRemoveAccess, string userId);
        Task ChangeFilePrivacyAsync(string fileId, string userId);
        Task ChangeFileNameAsync(string fileId, string newName, string userId);
        Task<DownloadModel> DownloadFileAsync(string fileId, string userId = null);
    }
}
