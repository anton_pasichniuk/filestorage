﻿using AutoMapper;
using BLL.Models;
using BLL.Services.Interfaces;
using BLL.Validation;
using DAL.UnitOfWork.Interfaces;
using Google.Apis.Drive.v3;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using User = DAL.Entities.User;

namespace BLL.Services.Implementation
{
    /// <summary>
    /// Service which provide access to user
    /// </summary>
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly DriveService _driveService;
        private readonly IMapper _mapper;

        /// <summary>
        /// Constructor injects all necessary components
        /// </summary>
        /// <param name="unitOfWork">Access to repositories</param>
        /// <param name="userManager">Identity manager for user</param>
        /// <param name="signInManager">Identity manager for sign in user</param>
        /// <param name="driveService">Service to manage files</param>
        /// <param name="mapper">Automapper</param>
        public UserService(IUnitOfWork unitOfWork,
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            DriveService driveService,
            IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _userManager = userManager;
            _signInManager = signInManager;
            _driveService = driveService;
            _mapper = mapper;
        }

        /// <summary>
        /// Validates UserModel and adds user to Db
        /// </summary>
        /// <exception cref="FileStorageException">
        /// Thown when:
        ///     1) Validation of UserModel has been failed
        ///     2) Unable to update info in Db
        /// </exception>
        /// <param name="model">UserModel, which is being added</param>
        public async Task AddAsync(UserModel model)
        {
            try
            {
                await ValidateUserToRegister(model);

                var user = CreateUserFromModel(model);               
                var result = await _userManager.CreateAsync(user, model.Password);

                if (!result.Succeeded)
                {
                    ThrowIdentityErrors(result.Errors);
                }

                user = await _userManager.FindByEmailAsync(user.Email);
                await _userManager.AddToRoleAsync(user, "User");
                await _unitOfWork.SaveAsync();                               
            }
            catch (FileStorageException e)
            {
                throw e;
            }
            catch (Exception)
            {
                throw new FileStorageException("User hasn't been registered");
            }
        }

        /// <summary>
        /// Converts UserModel to User
        /// </summary>
        /// <param name="model">UserModel to convert</param>
        /// <returns>Converted User</returns>
        private User CreateUserFromModel(UserModel model)
        {
            return new User
            {
                UserName = model.UserName,
                Email = model.Email,
                PhoneNumber = model.PhoneNumber
            };
        }

        /// <summary>
        /// Select all Identity errors details and throw the expection
        /// </summary>
        /// <param name="result">IEnumerable of IdentityErrors</param>
        private void ThrowIdentityErrors(IEnumerable<IdentityError> result)
        {
            string errors = string.Join("\n", result.Select(r => r.Description));
            throw new FileStorageException(errors);
        }

        /// <summary>
        /// Deletes user and their file by userId
        /// </summary>
        /// <exception cref="FileStorageException">
        /// Thrown when:
        ///     1) UserId doesn't exist
        ///     2) Errors to delete files from Google Drive
        ///     3) Errors to delete files info from Db
        /// </exception>
        /// <param name="userId">User id, who is being deleted</param>      
        public async Task DeleteByIdAsync(string userId)
        {
            try
            {
                await ValidateUserId(userId);

                var userFiles = _unitOfWork.FileUserRepository.GetFilesOfUserById(userId)
                    .Select(fu => fu.FileId)
                    .ToList();

                await RemoveUserFilesFromDrive(userFiles);
                await RemoveUserFilesFromDb(userFiles);
                
                var user = await _userManager.FindByIdAsync(userId);
                var result = await _userManager.DeleteAsync(user);

                if (!result.Succeeded)
                {
                    ThrowIdentityErrors(result.Errors);
                }

                await _unitOfWork.SaveAsync();
            }
            catch (FileStorageException e)
            {
                throw e;
            }
            catch (Exception)
            {
                throw new FileStorageException("User hasn't been deleted");
            }
        }

        /// <summary>
        /// Removes from Google Drive files by their Ids
        /// </summary>
        /// <param name="fileIds">Files Ids to remove</param>        
        private async Task RemoveUserFilesFromDrive(List<string> fileIds)
        {
            if (fileIds == null || fileIds.Count == 0)
                return;

            try
            {
                foreach(var fileId in fileIds)
                {
                    var deleteRequest = _driveService.Files.Delete(fileId);
                    await deleteRequest.ExecuteAsync();
                }
            }
            catch(Exception)
            {
                throw new FileStorageException("User hasn't been deleted, because of trouble of file deleting");
            }
        }

        /// <summary>
        /// Removes from Db files by their Ids
        /// </summary>
        /// <param name="fileIds">Files Ids to remove</param>
        private async Task RemoveUserFilesFromDb(List<string> fileIds)
        {
            if (fileIds == null || fileIds.Count < 0)
                return;

            try
            {
                foreach (var fileId in fileIds)
                {
                    await _unitOfWork.FileRepository.DeleteByIdAsync(fileId);
                }
            }
            catch (Exception)
            {
                throw new FileStorageException("User hasn't been deleted, because of trouble of file deleting");
            }
        }

        /// <summary>
        /// Finds user in Db by their email and password 
        /// </summary>
        /// <param name="email">Email of user to find</param>
        /// <param name="password">Password of user to find</param>
        /// <returns>UserModel, if user have been found or 'null', if not</returns>
        public async Task<UserModel> FindByEmailAndPasswordAsync(string email, string password)
        {
            var user = _userManager.Users.SingleOrDefault(u => u.Email == email);           

            if (user != null)
            {
                var signInResult = await _signInManager.CheckPasswordSignInAsync(user, password, false);

                if (signInResult.Succeeded)
                    return _mapper.Map<UserModel>(user);
            }

            return null;
        }

        /// <summary>
        /// Gets user by their Id
        /// </summary>
        /// <param name="id">User Id</param>
        /// <returns>UserModel, if user have been found or 'null', if not</returns>
        public async Task<UserModel> GetByIdAsync(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            var userModel = _mapper.Map<UserModel>(user);

            return userModel;
        }

        /// <summary>
        /// Gets all users from Db and converts them to UserModel
        /// </summary>
        /// <returns>IEnumerable of UserModel</returns>
        public List<UserModel> GetAll()
        {
            var users = _userManager.Users.ToList();
            var usersModel = _mapper.Map<List<UserModel>>(users);

            return usersModel;
        }

        /// <summary>
        /// Get roles of user by their Id
        /// </summary>
        /// <param name="id">User Id to find roles</param>
        /// <returns>IList of roles type of string</returns>
        public async Task<IList<string>> GetRolesAsync(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            var userRoles = await _userManager.GetRolesAsync(user);

            return userRoles;
        }

        /// <summary>
        /// Checks if user with such <paramref name="email"/> is already exists
        /// </summary>
        /// <param name="email">Email of user</param>
        /// <returns>True, if user exists or false, if not</returns>
        public async Task<bool> IsEmailExistsAsync(string email)
        {
            var user = await _userManager.FindByEmailAsync(email);
            return user != null;
        }

        /// <summary>
        /// Checks uniqueness of user data
        /// </summary>
        /// <param name="user">User data</param>
        private async Task ValidateUserToRegister(UserModel user)
        {
            if (await IsEmailExistsAsync(user.Email))
                throw new FileStorageException("Email is already exists");
            if (_userManager.Users.Any(u => u.Email == user.Email))
                throw new FileStorageException("Username is already exists");
            if (_userManager.Users.Any(u => u.PhoneNumber == user.PhoneNumber))
                throw new FileStorageException("Phone number is already exists");
        }

        /// <summary>
        /// Update data of user by their Id
        /// </summary>
        /// <exception cref="FileStorageException">
        /// Thown when:
        ///     1) User doesn't exist
        ///     2) User info isn't unique
        ///     3) Errors to update user in Db
        /// </exception>
        /// <param name="userId">Id of user, who is being updated</param>
        /// <param name="newUserInfo">UpdateUserModel that contains new info about user</param>
        public async Task UpdateAsync(string userId, UpdateUserModel newUserInfo)
        {
            try
            {
                await ValidateUserId(userId);
                ValidateInfoToUpdate(newUserInfo);

                var user = await _userManager.FindByIdAsync(userId);

                ChangeInfo(user, newUserInfo);

                var result = await _userManager.UpdateAsync(user);

                if (!result.Succeeded)
                {
                    ThrowIdentityErrors(result.Errors);
                }

                await _unitOfWork.SaveAsync();
            }
            catch (FileStorageException e)
            {
                throw e;
            }
            catch (Exception)
            {
                throw new FileStorageException("User hasn't been updated");
            }
        }

        /// <summary>
        /// Find if user exists and throws error if not
        /// </summary>
        /// <param name="userId">Id of user</param>
        private async Task ValidateUserId(string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);

            if (user == null)
                throw new FileStorageException("User doesn't exist");
        }

        /// <summary>
        /// Checks uniquenesses of user info
        /// </summary>
        /// <param name="userName">Username of user</param>
        private void ValidateInfoToUpdate(UpdateUserModel userInfo)
        {
            if (_userManager.Users.Any(u => u.UserName == userInfo.UserName))
                throw new FileStorageException("User with such name is already exists");
            if(_userManager.Users.Any(u => u.PhoneNumber == userInfo.PhoneNumber))
                throw new FileStorageException("User with such phone number is already exists");
        }

        /// <summary>
        /// Changes user info
        /// </summary>
        /// <param name="user">User, who info is being changed</param>
        /// <param name="userInfo">New user info</param>
        private void ChangeInfo(User user, UpdateUserModel userInfo)
        {
            if (!string.IsNullOrEmpty(userInfo.UserName))
                user.UserName = userInfo.UserName;
            if (!string.IsNullOrEmpty(userInfo.PhoneNumber))
                user.PhoneNumber = userInfo.PhoneNumber;
        }
    }
}
