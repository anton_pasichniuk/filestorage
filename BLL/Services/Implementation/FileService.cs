﻿using AutoMapper;
using BLL.Models;
using BLL.Services.Interfaces;
using BLL.Validation;
using DAL.Entities;
using DAL.UnitOfWork.Interfaces;
using Google.Apis.Download;
using Google.Apis.Drive.v3;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.StaticFiles;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Services.Implementation
{
    /// <summary>
    /// Service which provide access to files
    /// </summary>
    public class FileService : IFileService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly DriveService _driveService;
        private readonly UserManager<User> _userManager;
        private readonly IMapper _mapper;

        /// <summary>
        /// Constructor injects all necessary components
        /// </summary>
        /// <param name="unitOfWork">Access to repositories</param>
        /// <param name="driveService">Service to manage files </param>
        /// <param name="userManager">Identity manager for user</param>
        /// <param name="mapper">Automapper</param>
        public FileService(IUnitOfWork unitOfWork,
            DriveService driveService,
            UserManager<User> userManager,
            IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;            
            _driveService = driveService;
            _userManager = userManager;
        }

        /// <summary>
        /// Get files from Db that can be accessed by user:
        ///     'Guest' - files with public access
        ///     'User' - files with public access and shared files
        ///     'Admin' - all files
        /// </summary>
        /// <param name="userId">
        /// Id of user from Identity User table
        ///     If it's 'null' - user role is 'Guest'
        ///     If not 'null' - user role is 'User' or 'Admin'
        /// </param>
        /// <returns>List of FileUserModel, which contains FilesInfo and additional info about owner of file</returns>
        public async Task<List<FileUserModel>> GetFilesAsync(string userId = null)
        {
            var filesQueryable = _unitOfWork.FileUserRepository.FindAllWithDetails();

            if(userId == null)
            {
                filesQueryable = filesQueryable.Where(fu => fu.IsPublic == true);
            }
            else
            {
                var user = await _userManager.FindByIdAsync(userId);
                
                if(await _userManager.IsInRoleAsync(user, "User"))
                {
                    var fileAccess = _unitOfWork.FileAccessRepository.GetFileAccessByUserId(userId);

                    filesQueryable = filesQueryable.Where(fu => fu.IsPublic == true ||
                        fileAccess.Any(fa => fa.FileId == fu.FileId));
                }
            }

            var filesUserModels = _mapper.Map<List<FileUserModel>>(filesQueryable.ToList());

            return filesUserModels;
        }

        /// <summary>
        /// Get file, which have been created by user, from Db
        /// </summary>
        /// <param name="userId">Id of user from Identity User table</param>
        /// <returns>List of FileModel, which contains FilesInfo</returns>
        public List<FileModel> GetUserFiles(string userId)
        {
            var userFiles = _unitOfWork.FileUserRepository.FindAllWithDetails()
                .Where(fu => fu.UserId == userId);

            var fileModel = _mapper.Map<List<FileModel>>(userFiles);

            return fileModel;
        }

        /// <summary>
        /// Get files from Db, which have been shared with user
        /// </summary>
        /// <param name="userId">Id of user from Identity User table</param>
        /// <returns>List of FileUserModel, which contains FilesInfo and additional info about owner of file</returns>
        public List<FileUserModel> GetFilesWithAccess(string userId)
        {
            var filesWithAccess = _unitOfWork.FileAccessRepository
                .GetFileAccessByUserId(userId);

            var userFiles = _unitOfWork.FileUserRepository.FindAllWithDetails()
                .Where(fu => filesWithAccess.Any(fa => fa.FileId == fu.FileId));

            var fileUserModels = _mapper.Map<List<FileUserModel>>(userFiles);

            return fileUserModels;
        }

        /// <summary>
        /// Creates new file in Google Drive and file info in Db
        /// </summary>
        /// <exception cref="FileStorageException">
        /// Thown when:
        ///     1) Unable to save file in Google Drive or Db       
        /// </exception>
        /// <param name="fileToUpload">File to upload</param>
        /// <param name="userId">User which creates file</param>
        public async Task AddAsync(IFormFile fileToUpload, string userId)
        {
            try
            {
                var file = new Google.Apis.Drive.v3.Data.File
                {
                    Name = fileToUpload.FileName,
                    MimeType = fileToUpload.ContentType
                };

                var request = _driveService.Files.Create(file, fileToUpload.OpenReadStream(), fileToUpload.ContentType);
                request.Fields = "id, name, size, version, createdTime";
                await request.UploadAsync();

                var driveFile = request.ResponseBody;

                var filesInfo = _mapper.Map<FilesInfo>(driveFile);

                var fileUser = new FileUser
                {
                    FileId = filesInfo.Id,
                    UserId = userId,
                    IsPublic = false
                };

                await _unitOfWork.FileRepository.AddAsync(filesInfo);
                await _unitOfWork.FileUserRepository.AddAsync(fileUser);
                await _unitOfWork.SaveAsync();
            }
            catch (FileStorageException e)
            {
                throw e;
            }
            catch (Exception)
            {
                throw new FileStorageException("File hasn't been created");
            }
        }

        /// <summary>
        /// Change file privacy(public or private)
        /// </summary>
        /// <exception cref="FileStorageException">
        /// Thown when:
        ///     1) File doens`t exists
        ///     2) User doesn`t have access to change file privacy
        ///     3) Unable to update info in Db
        /// </exception>
        /// <param name="fileId">File id, which privacy do you want to change</param>
        /// <param name="userId">User id, who wants to change file privacy</param>
        public async Task ChangeFilePrivacyAsync(string fileId, string userId)
        {
            try
            {
                await ValidateIfFileExists(fileId);
                await ValidateIfUserFilesOwnerOrAdmin(fileId, userId);

                var fileUser = _unitOfWork.FileUserRepository.FindByFileId(fileId);

                fileUser.IsPublic = !fileUser.IsPublic;

                _unitOfWork.FileUserRepository.Update(fileUser);

                await _unitOfWork.SaveAsync();
            }
            catch (FileStorageException e)
            {
                throw e;
            }
            catch (Exception)
            {
                throw new FileStorageException("File privacy hasn't been changed");
            }
        }

        /// <summary>
        /// Changes name of file
        /// </summary>
        /// <exception cref="FileStorageException">
        ///  Thown when:
        ///     1) File doens`t exists
        ///     2) User doesn`t have access to change file name
        ///     3) Errors to update info in Db
        /// </exception>
        /// <param name="fileId">File id to change name</param>
        /// <param name="newName">New name of file</param>
        /// <param name="userId">User id, who tries to change file name</param>
        public async Task ChangeFileNameAsync(string fileId, string newName, string userId)
        {
            try
            {
                await ValidateIfFileExists(fileId);
                await ValidateIfUserFilesOwnerOrAdmin(fileId, userId);

                var file = await _driveService.Files.Get(fileId).ExecuteAsync();
                file.Name = newName;
                file.Id = null;

                var request = _driveService.Files.Update(file, fileId);
                request.Fields = "id, name, size, version, createdTime";
                var responseFile = await request.ExecuteAsync();

                var newFile = _mapper.Map<FilesInfo>(responseFile);

                _unitOfWork.FileRepository.Update(newFile);
                await _unitOfWork.SaveAsync();
            }
            catch (FileStorageException e)
            {
                throw e;
            }
            catch (Exception)
            {
                throw new FileStorageException("File name hasn't been changed");
            }
        }

        /// <summary>
        /// Gives access for user (by username) to file
        /// </summary>
        /// <exception cref="FileStorageException">
        /// Thorw when:
        ///     1) File doens`t exists
        ///     2) User(who tries to give access) doesn`t have permissions to give access
        ///     3) Username of access recipient doen`t exist
        ///     4) User, who access is being given, already have access to file
        ///     5) Errors to update info in Db
        /// </exception>
        /// <param name="fileId">File id to give access to</param>
        /// <param name="userNameToGiveAccess">Username, who access to file is being given</param>
        /// <param name="userId">User id, who tries to give access to file</param>
        public async Task GiveAccessAsync(string fileId, string userNameToGiveAccess, string userId)
        {
            try
            {
                await ValidateIfFileExists(fileId);
                await ValidateIfUserFilesOwnerOrAdmin(fileId, userId);
                await ValidateToGiveAccess(fileId, userNameToGiveAccess);

                var user = await _userManager.FindByNameAsync(userNameToGiveAccess);

                AccessToFile accessToFile = new AccessToFile
                {
                    FileId = fileId,
                    UserId = user.Id
                };

                await _unitOfWork.FileAccessRepository.AddAsync(accessToFile);
                await _unitOfWork.SaveAsync();
            }
            catch (FileStorageException e)
            {
                throw e;
            }
            catch (Exception)
            {
                throw new FileStorageException("Access hasn't been given");
            }
        }

        /// <summary>
        /// Remove access to file from user by his username 
        /// </summary>
        /// /// <exception cref="FileStorageException">
        /// Thorw when:
        ///     1) File doens`t exists
        ///     2) User(who tries to remove access) doesn`t have permissions to give access
        ///     3) Username of access recipient doen`t exist
        ///     4) User, who access is being removed, doesn`t have access to file
        ///     5) Errors to update info in Db
        /// </exception>
        /// <param name="fileId">File id to give access to</param>
        /// <param name="userNameToRemoveAccess">Username, who access to file is being removed</param>
        /// <param name="userId">User id, who tries to remove access to file</param>
        public async Task RemoveAccessAsync(string fileId, string userNameToRemoveAccess, string userId)
        {
            try
            {
                await ValidateIfFileExists(fileId);
                await ValidateIfUserFilesOwnerOrAdmin(fileId, userId);
                await ValidateToRemoveAccess(fileId, userNameToRemoveAccess);

                var user = await _userManager.FindByNameAsync(userNameToRemoveAccess);

                var fileAccess = _unitOfWork.FileAccessRepository.FindByUserIdAndFileId(user.Id, fileId);

                await _unitOfWork.FileAccessRepository.DeleteByIdAsync(fileAccess.Id);
                await _unitOfWork.SaveAsync();
            }
            catch (FileStorageException e)
            {
                throw e;
            }
            catch (Exception)
            {
                throw new FileStorageException("File name hasn't been changed");
            }
        }

        /// <summary>
        /// Downloads file from Google Drive
        /// </summary>
        /// <exception cref="FileStorageException">
        /// Thorw when:
        ///     1) File doens`t exists
        ///     2) User unauthorized
        ///     3) User with such id doesn't exist
        ///     4) User doesn't have access to file
        ///     5) Errors to get file from Google Drive 
        /// </exception>
        /// <param name="fileId">File id, which is downloaded</param>
        /// <param name="userId">User id, who ties to download file</param>   
        /// <returns>DownloadModel, which contains info about file</returns>
        public async Task<DownloadModel> DownloadFileAsync(string fileId, string userId = null)
        {
            try
            {
                await ValidateIfFileExists(fileId);
                await ValidateToDownloadFile(fileId, userId);                

                var request = _driveService.Files.Get(fileId);

                var file = await request.ExecuteAsync();
                
                DownloadModel downloadModel = new DownloadModel();

                downloadModel.Name = file.Name;
                downloadModel.ContentType = file.MimeType;
                request.Download(downloadModel.Memory);

                return downloadModel;
            }
            catch (FileStorageException e)
            {
                throw e;
            }
            catch (Exception)
            {
                throw new FileStorageException("Download failed");
            }
        }

        /// <summary>
        /// Deletes file from Db and Google Drive
        /// </summary>
        /// <exception cref="FileStorageException">
        /// Thorw when:
        ///     1) File doesn`t exists
        ///     2) User doesn't have access to file
        ///     3) Errors to delete file info from Db
        ///     4) Errors to delete file from Google Drive
        /// </exception>
        /// <param name="fileId">File id, which should be deleted</param>
        /// <param name="userId">User id, who tries to delete file</param>
        public async Task DeleteByIdAsync(string fileId, string userId)
        {
            try
            {
                await ValidateIfFileExists(fileId);
                await ValidateIfUserFilesOwnerOrAdmin(fileId, userId);

                var request = _driveService.Files.Delete(fileId);
                request.Fields = "id";
                await request.ExecuteAsync();

                await _unitOfWork.FileRepository.DeleteByIdAsync(fileId);

                await _unitOfWork.SaveAsync();
            }
            catch (FileStorageException e)
            {
                throw e;
            }
            catch (Exception)
            {
                throw new FileStorageException("File hasn't been deleted");
            }
        }

        /// <summary>
        /// Validates if user is owner of file and throws errors if not
        /// </summary>
        /// <param name="fileId">Id of file</param>
        /// <param name="userId">Id of user, who should be owner of file</param>
        private async Task ValidateIfUserFilesOwnerOrAdmin(string fileId, string userId)
        {
            bool isHavePermission = true;

            var fileUser = _unitOfWork.FileUserRepository.FindByUserIdAndFileId(userId, fileId);

            if (fileUser == null)
            {
                var user = await _userManager.FindByIdAsync(userId);
                isHavePermission = await _userManager.IsInRoleAsync(user, "Admin");
            }

            if (!isHavePermission)
                throw new FileStorageException("You don`t have access to file");
        }

        /// <summary>
        /// Validates if file exists and throws error if not
        /// </summary>
        /// <param name="fileId">Id of file</param>
        /// <returns></returns>
        private async Task ValidateIfFileExists(string fileId)
        {
            var file = await _unitOfWork.FileRepository.GetByIdAsync(fileId);

            if (file == null)
                throw new FileStorageException("File doesn't exist");
        }

        /// <summary>
        /// Validates if user exists and if they already had access. Throws errors, if validation failed
        /// </summary>
        /// <param name="fileId">Id of file</param>
        /// <param name="userName">Username of person, who is being given access</param>
        private async Task ValidateToGiveAccess(string fileId, string userName)
        {
            var user = await _userManager.FindByNameAsync(userName);
            if (user == null)
                throw new FileStorageException("There is no such user");

            var fileUser = _unitOfWork.FileUserRepository.FindByUserIdAndFileId(user.Id, fileId);
            if (fileUser != null)
                throw new FileStorageException("User already have access to file");

            var fileAccess = _unitOfWork.FileAccessRepository.FindByUserIdAndFileId(user.Id, fileId);
            if (fileAccess != null)
                throw new FileStorageException("User already have access to file");
        }

        /// <summary>
        /// Validates if user exists and if they has access to file to remove. Throws errors, if validation failed
        /// </summary>
        /// <param name="fileId">Id of file</param>
        /// <param name="userName">Id of user to remove access</param>
        private async Task ValidateToRemoveAccess(string fileId, string userName)
        {            
            var user = await _userManager.FindByNameAsync(userName);
            if (user == null)
                throw new FileStorageException("There is no such user");

            var fileAccess = _unitOfWork.FileAccessRepository.FindByUserIdAndFileId(user.Id, fileId);
            if (fileAccess == null)
                throw new FileStorageException("User doesn't have access to file");
        }

        /// <summary>
        /// Validates if user and file exists and if user has access to download file. Throws errors, if validation failed
        /// </summary>
        /// <param name="fileId">Id of file to download</param>
        /// <param name="userId">Id of user, who is downloading file</param>
        /// <returns></returns>
        private async Task ValidateToDownloadFile(string fileId, string userId)
        {            
            var fileUser = _unitOfWork.FileUserRepository.FindByFileId(fileId);

            if (fileUser.IsPublic)
                return;

            if (userId == null)
                throw new FileStorageException("You are not authorized");

            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
                throw new FileStorageException("There is no such user");

            if (fileUser.UserId == user.Id)
                return;

            var isHaveAccess = _unitOfWork.FileAccessRepository.GetFileAccessByUserId(userId)
                .Any(fa => fa.FileId == fileId);

            if (!isHaveAccess)
                throw new FileStorageException("You don`t have access to this file");
        }
    }
}
