﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Validation
{
    public class FileStorageException : Exception
    {
        public FileStorageException() : base()
        {
        }

        public FileStorageException(string message) : base(message)
        {
        }

        public FileStorageException(string message, Exception inner) : base(message, inner)
        {
        }
    }
}
