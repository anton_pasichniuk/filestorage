﻿using AutoMapper;
using BLL.Models;
using DAL.Entities;
using Google.Apis.Drive.v3.Data;

namespace BLL
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<DAL.Entities.User, UserModel>()
                .ForMember(dest => dest.Password,
                    opt => opt.Ignore())
                .ReverseMap();

            CreateMap<FileUser, FileUserModel>()
                .ForMember(dest => dest.Name,
                    opt => opt.MapFrom(src => src.File.Name))
                .ForMember(dest => dest.Size,
                    opt => opt.MapFrom(src => src.File.Size))
                .ForMember(dest => dest.Version,
                    opt => opt.MapFrom(src => src.File.Version))
                .ForMember(dest => dest.CreatedDate,
                    opt => opt.MapFrom(src => src.File.CreatedDate))
                .ForMember(dest => dest.UserName,
                    opt => opt.MapFrom(src => src.User.UserName))
                .ReverseMap();

            CreateMap<File, FilesInfo>()
                .ForMember(dest => dest.CreatedDate,
                    opt => opt.MapFrom(src => src.CreatedTime))
                .ReverseMap();

            CreateMap<AccessToFile, FileModel>()
                .ForMember(dest => dest.Name,
                    opt => opt.MapFrom(src => src.File.Name))
                .ForMember(dest => dest.Size,
                    opt => opt.MapFrom(src => src.File.Size))
                .ForMember(dest => dest.Version,
                    opt => opt.MapFrom(src => src.File.Version))
                .ForMember(dest => dest.CreatedDate,
                    opt => opt.MapFrom(src => src.File.CreatedDate))
                .ReverseMap();

            CreateMap<FileUser, FileModel>()
                .ForMember(dest => dest.Name,
                    opt => opt.MapFrom(src => src.File.Name))
                .ForMember(dest => dest.Size,
                    opt => opt.MapFrom(src => src.File.Size))
                .ForMember(dest => dest.Version,
                    opt => opt.MapFrom(src => src.File.Version))
                .ForMember(dest => dest.CreatedDate,
                    opt => opt.MapFrom(src => src.File.CreatedDate))
                .ReverseMap();
        }
    }
}
