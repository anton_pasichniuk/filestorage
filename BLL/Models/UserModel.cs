﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BLL.Models
{
    public class UserModel
    {
        public string Id { get; set; }
        [Required(AllowEmptyStrings = false)]
        [MinLength(6), MaxLength(25)]
        public string UserName { get; set; }
        [Required(AllowEmptyStrings = false)]
        [EmailAddress]
        public string Email { get; set; }
        [Required(AllowEmptyStrings = false)]
        [MinLength(6), MaxLength(25)]     
        public string Password { get; set; }
        [Required(AllowEmptyStrings = false)]
        [Phone]
        public string PhoneNumber { get; set; }
    }
}
