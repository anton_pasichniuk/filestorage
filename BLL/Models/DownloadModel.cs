﻿using System.IO;

namespace BLL.Models
{
    public class DownloadModel
    {
        public string Name { get; set; }
        public string ContentType { get; set; }
        public MemoryStream Memory { get; set; }

        public DownloadModel()
        {
            Memory = new MemoryStream();
        }
    }
}
