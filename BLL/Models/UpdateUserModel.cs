﻿using System.ComponentModel.DataAnnotations;

namespace BLL.Models
{
    public class UpdateUserModel
    {
        [MinLength(6), MaxLength(25)]
        public string UserName { get; set; }
        [Phone]
        public string PhoneNumber { get; set; }
    }
}
