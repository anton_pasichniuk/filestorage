﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BLL.Models
{
    public class FileUserModel
    {
        public string FileId { get; set; }
        [Required]
        public string Name { get; set; }
        public long? Size { get; set; }
        public long? Version { get; set; }
        public DateTime? CreatedDate { get; set; }
        [Required]
        public string UserName { get; set; }
        public bool IsPublic { get; set; }
    }
}
