﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models
{
    public class FileModel
    {
        public string FileId { get; set; }
        public string Name { get; set; }
        public long? Size { get; set; }
        public long? Version { get; set; }
        public DateTime? CreatedDate { get; set; }
        public bool IsPublic { get; set; }
    }
}
