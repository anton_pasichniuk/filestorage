﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DAL.Entities
{
    public class FileUser
    {        
        public int Id { get; set; }
        [Required]
        public string FileId { get; set; }
        [Required]
        public string UserId { get; set; }
        [Required]
        public bool IsPublic { get; set; }

        public virtual User User { get; set; }
        public virtual FilesInfo File { get; set; }
    }
}
