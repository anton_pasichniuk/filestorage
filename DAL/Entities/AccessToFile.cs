﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DAL.Entities
{
    public class AccessToFile
    {
        public int Id { get; set; }
        [Required]
        public string FileId { get; set; }
        [Required]
        public string UserId { get; set; }

        public virtual User User { get; set; }
        public virtual FilesInfo File { get; set; }
    }
}
