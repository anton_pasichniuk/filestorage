﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public class FilesInfo
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public long? Size { get; set; }
        public long? Version { get; set; }
        public DateTime? CreatedDate { get; set; }

        public virtual FileUser FilesUsers { get; set; }
        public virtual ICollection<AccessToFile> FileAccesses { get; set; }
    }
}
