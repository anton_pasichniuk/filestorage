﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public class User : IdentityUser
    {
        public virtual ICollection<FileUser> FilesUsers { get; set; }
        public virtual ICollection<AccessToFile> FileAccesses { get; set; }
    }
}
