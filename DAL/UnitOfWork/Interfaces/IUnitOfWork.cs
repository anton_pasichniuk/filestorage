﻿using DAL.Entities;
using DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.UnitOfWork.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IFileRepository FileRepository { get; }
        IFileUserRepository FileUserRepository { get; }
        IFileAccessRepository FileAccessRepository { get; }

        Task<int> SaveAsync();
    }
}
