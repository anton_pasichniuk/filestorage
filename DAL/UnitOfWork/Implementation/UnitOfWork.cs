﻿using DAL.EF;
using DAL.Entities;
using DAL.Repositories.Implementation;
using DAL.Repositories.Interfaces;
using DAL.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.UnitOfWork.Implementation
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly FileStorageContext _context;

        private IFileRepository fileRepository;
        private IFileUserRepository fileUserRepository;
        private IFileAccessRepository fileAccessRepository;

        public UnitOfWork(FileStorageContext context)
        {
            _context = context;           
        }

        public IFileRepository FileRepository
        {
            get
            {
                if (fileRepository == null)
                    fileRepository = new FileRepository(_context);
                return fileRepository;
            }
        }

        public IFileUserRepository FileUserRepository
        {
            get
            {
                if (fileUserRepository == null)
                    fileUserRepository = new FileUserRepository(_context);
                return fileUserRepository;
            }
        }

        public IFileAccessRepository FileAccessRepository
        {
            get
            {
                if (fileAccessRepository == null)
                    fileAccessRepository = new FileAccessRepository(_context);
                return fileAccessRepository;
            }
        }

        #region IDisposable realization

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

        public Task<int> SaveAsync()
        {
            return _context.SaveChangesAsync();
        }
    }
}
