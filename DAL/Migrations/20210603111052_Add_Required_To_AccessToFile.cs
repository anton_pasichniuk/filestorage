﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class Add_Required_To_AccessToFile : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FileAccess_AspNetUsers_UserId",
                table: "FileAccess");

            migrationBuilder.DropForeignKey(
                name: "FK_FileAccess_Files_FileId",
                table: "FileAccess");

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "FileAccess",
                type: "nvarchar(450)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "FileId",
                table: "FileAccess",
                type: "nvarchar(450)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b74ddd14-6340-4840-95c2-db12554843e5",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "b5e858c8-a0fe-4709-9f32-a84fb7cc3350", "AQAAAAEAACcQAAAAEIs0huNM4mouMSjao+2nx+zdNJ+7nH96o/fJEh7jD8HLQMOs0KRFI9tuAzGN2XudMA==", "b2a5ebb4-857f-4f5d-8f91-c2de4b64925f" });

            migrationBuilder.AddForeignKey(
                name: "FK_FileAccess_AspNetUsers_UserId",
                table: "FileAccess",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FileAccess_Files_FileId",
                table: "FileAccess",
                column: "FileId",
                principalTable: "Files",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FileAccess_AspNetUsers_UserId",
                table: "FileAccess");

            migrationBuilder.DropForeignKey(
                name: "FK_FileAccess_Files_FileId",
                table: "FileAccess");

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "FileAccess",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)");

            migrationBuilder.AlterColumn<string>(
                name: "FileId",
                table: "FileAccess",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b74ddd14-6340-4840-95c2-db12554843e5",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "c2a3e3a2-754e-4c99-bbf8-844842160906", "AQAAAAEAACcQAAAAEDH8Yq84+EiK836LK2vU775d/ckmvgirzxt9SeaNzZkbu7r1JMQ0sl5+lQHuDxexMA==", "29b59047-620c-4c96-ade9-7eaed3cc243b" });

            migrationBuilder.AddForeignKey(
                name: "FK_FileAccess_AspNetUsers_UserId",
                table: "FileAccess",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_FileAccess_Files_FileId",
                table: "FileAccess",
                column: "FileId",
                principalTable: "Files",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
