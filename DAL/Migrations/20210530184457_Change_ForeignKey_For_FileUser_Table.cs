﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class Change_ForeignKey_For_FileUser_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_FileUser_FileId",
                table: "FileUser");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b74ddd14-6340-4840-95c2-db12554843e5",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "c2a3e3a2-754e-4c99-bbf8-844842160906", "AQAAAAEAACcQAAAAEDH8Yq84+EiK836LK2vU775d/ckmvgirzxt9SeaNzZkbu7r1JMQ0sl5+lQHuDxexMA==", "29b59047-620c-4c96-ade9-7eaed3cc243b" });

            migrationBuilder.CreateIndex(
                name: "IX_FileUser_FileId",
                table: "FileUser",
                column: "FileId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_FileUser_FileId",
                table: "FileUser");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b74ddd14-6340-4840-95c2-db12554843e5",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "2591e1ca-03ee-4dfd-994e-79dbc24b8652", "AQAAAAEAACcQAAAAEHx5087XZxATtO3jWDV3oFdm9V8y9sSamcM561r/5aQ44X9KY7VU8YgLxu8/fgXBlA==", "ea85f3b1-0835-4430-bd02-cfb5c1497027" });

            migrationBuilder.CreateIndex(
                name: "IX_FileUser_FileId",
                table: "FileUser",
                column: "FileId");
        }
    }
}
