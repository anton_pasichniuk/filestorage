﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class Change_ValueType_Of_FilesInfo_Id : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Files",
                table: "Files");

            migrationBuilder.DropColumn(
                name: "FilesInfoId",
                table: "Files");

            migrationBuilder.AlterColumn<string>(
                name: "FileId",
                table: "FileUser",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Id",
                table: "Files",
                type: "nvarchar(450)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<string>(
                name: "FileId",
                table: "FileAccess",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Files",
                table: "Files",
                column: "Id");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b74ddd14-6340-4840-95c2-db12554843e5",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "0f2fe130-f372-4873-9604-d14cee134717", "AQAAAAEAACcQAAAAEGYiI8vHTYQnQncyHETeYq8lninav9CfscH02EKbF6G14t6dVR8AlSE+L3Ouug/UAA==", "ac4870a3-3cf8-4914-89fe-c6f58adf40b3" });

            migrationBuilder.CreateIndex(
                name: "IX_FileUser_FileId",
                table: "FileUser",
                column: "FileId");

            migrationBuilder.CreateIndex(
                name: "IX_FileAccess_FileId",
                table: "FileAccess",
                column: "FileId");

            migrationBuilder.AddForeignKey(
                name: "FK_FileAccess_Files_FileId",
                table: "FileAccess",
                column: "FileId",
                principalTable: "Files",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_FileUser_Files_FileId",
                table: "FileUser",
                column: "FileId",
                principalTable: "Files",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FileAccess_Files_FileId",
                table: "FileAccess");

            migrationBuilder.DropForeignKey(
                name: "FK_FileUser_Files_FileId",
                table: "FileUser");

            migrationBuilder.DropIndex(
                name: "IX_FileUser_FileId",
                table: "FileUser");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Files",
                table: "Files");

            migrationBuilder.DropIndex(
                name: "IX_FileAccess_FileId",
                table: "FileAccess");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "Files");

            migrationBuilder.AlterColumn<string>(
                name: "FileId",
                table: "FileUser",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "FilesInfoId",
                table: "Files",
                type: "int",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AlterColumn<string>(
                name: "FileId",
                table: "FileAccess",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Files",
                table: "Files",
                column: "FilesInfoId");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b74ddd14-6340-4840-95c2-db12554843e5",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "9c682a7d-2818-4400-a37f-c88ab54f6584", "AQAAAAEAACcQAAAAEBiFdj42CpkL863RkuuN1mm++r2fevOSIUQTMWDOujdXWgdH+f7Qva1Y5hwXBjGu0g==", "d40e8c56-006f-47f6-a8cb-7152a573b809" });
        }
    }
}
