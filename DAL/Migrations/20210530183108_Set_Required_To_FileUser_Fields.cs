﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class Set_Required_To_FileUser_Fields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FileUser_AspNetUsers_UserId",
                table: "FileUser");

            migrationBuilder.DropForeignKey(
                name: "FK_FileUser_Files_FileId",
                table: "FileUser");

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "FileUser",
                type: "nvarchar(450)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "FileId",
                table: "FileUser",
                type: "nvarchar(450)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b74ddd14-6340-4840-95c2-db12554843e5",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "2591e1ca-03ee-4dfd-994e-79dbc24b8652", "AQAAAAEAACcQAAAAEHx5087XZxATtO3jWDV3oFdm9V8y9sSamcM561r/5aQ44X9KY7VU8YgLxu8/fgXBlA==", "ea85f3b1-0835-4430-bd02-cfb5c1497027" });

            migrationBuilder.AddForeignKey(
                name: "FK_FileUser_AspNetUsers_UserId",
                table: "FileUser",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FileUser_Files_FileId",
                table: "FileUser",
                column: "FileId",
                principalTable: "Files",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FileUser_AspNetUsers_UserId",
                table: "FileUser");

            migrationBuilder.DropForeignKey(
                name: "FK_FileUser_Files_FileId",
                table: "FileUser");

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "FileUser",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)");

            migrationBuilder.AlterColumn<string>(
                name: "FileId",
                table: "FileUser",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b74ddd14-6340-4840-95c2-db12554843e5",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "0f2fe130-f372-4873-9604-d14cee134717", "AQAAAAEAACcQAAAAEGYiI8vHTYQnQncyHETeYq8lninav9CfscH02EKbF6G14t6dVR8AlSE+L3Ouug/UAA==", "ac4870a3-3cf8-4914-89fe-c6f58adf40b3" });

            migrationBuilder.AddForeignKey(
                name: "FK_FileUser_AspNetUsers_UserId",
                table: "FileUser",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_FileUser_Files_FileId",
                table: "FileUser",
                column: "FileId",
                principalTable: "Files",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
