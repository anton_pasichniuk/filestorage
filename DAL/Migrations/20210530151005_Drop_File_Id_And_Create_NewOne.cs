﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class Drop_File_Id_And_Create_NewOne : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FileAccess_Files_FileId",
                table: "FileAccess");

            migrationBuilder.DropForeignKey(
                name: "FK_FileUser_Files_FileId",
                table: "FileUser");

            migrationBuilder.DropIndex(
                name: "IX_FileUser_FileId",
                table: "FileUser");

            migrationBuilder.DropIndex(
                name: "IX_FileAccess_FileId",
                table: "FileAccess");

            migrationBuilder.RenameColumn(
                name: "CreateDate",
                table: "Files",
                newName: "CreatedDate");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Files",
                newName: "FilesInfoId");

            migrationBuilder.AlterColumn<string>(
                name: "FileId",
                table: "FileUser",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<string>(
                name: "FileId",
                table: "FileAccess",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b74ddd14-6340-4840-95c2-db12554843e5",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "9c682a7d-2818-4400-a37f-c88ab54f6584", "AQAAAAEAACcQAAAAEBiFdj42CpkL863RkuuN1mm++r2fevOSIUQTMWDOujdXWgdH+f7Qva1Y5hwXBjGu0g==", "d40e8c56-006f-47f6-a8cb-7152a573b809" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "CreatedDate",
                table: "Files",
                newName: "CreateDate");

            migrationBuilder.RenameColumn(
                name: "FilesInfoId",
                table: "Files",
                newName: "Id");

            migrationBuilder.AlterColumn<int>(
                name: "FileId",
                table: "FileUser",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "FileId",
                table: "FileAccess",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b74ddd14-6340-4840-95c2-db12554843e5",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "a037fecc-7603-45b9-9cca-42495372e433", "AQAAAAEAACcQAAAAEEm8Q/4/5KeJFEuc55hb/RSiIzhDicloL8qWa11rBfY6p6wPe5B8YpmQWuF8I8RZOw==", "8965feff-2f0e-4e80-8b44-62a044cfe05b" });

            migrationBuilder.CreateIndex(
                name: "IX_FileUser_FileId",
                table: "FileUser",
                column: "FileId");

            migrationBuilder.CreateIndex(
                name: "IX_FileAccess_FileId",
                table: "FileAccess",
                column: "FileId");

            migrationBuilder.AddForeignKey(
                name: "FK_FileAccess_Files_FileId",
                table: "FileAccess",
                column: "FileId",
                principalTable: "Files",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FileUser_Files_FileId",
                table: "FileUser",
                column: "FileId",
                principalTable: "Files",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
