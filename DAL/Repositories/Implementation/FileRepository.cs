﻿using DAL.EF;
using DAL.Entities;
using DAL.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories.Implementation
{
    public class FileRepository : IFileRepository
    {
        private readonly FileStorageContext _context;
        private readonly DbSet<FilesInfo> _set;

        public FileRepository(FileStorageContext context)
        {
            _context = context;
            _set = context.Set<FilesInfo>();
        }

        public async Task AddAsync(FilesInfo entity)
        {
            await _set.AddAsync(entity);
        }

        public void Delete(FilesInfo entity)
        {
            _set.Remove(entity);
        }

        public async Task DeleteByIdAsync(string id)
        {
            var entity = await _set.FirstOrDefaultAsync(e => e.Id == id);
            _set.Remove(entity);
        }

        public IQueryable<FilesInfo> FindAll()
        {
            return _set;
        }

        public async Task<FilesInfo> GetByIdAsync(string id)
        {
            var entity = await _set.FirstOrDefaultAsync(e => e.Id == id);
            return entity;
        }

        public void Update(FilesInfo entity)
        {
            var file = _set.FirstOrDefault(f => f.Id == entity.Id);

            if (file != null)
            {
                file.Name = entity.Name;
                file.Size = entity.Size;
                file.Version = entity.Version;
                file.CreatedDate = entity.CreatedDate;
            }
            else
                throw new DbUpdateException("Entity doen't exist");
        }
    }
}
