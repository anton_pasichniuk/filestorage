﻿using DAL.EF;
using DAL.Entities;
using DAL.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories.Implementation
{
    public class FileAccessRepository : IFileAccessRepository
    {
        private readonly FileStorageContext _context;
        private readonly DbSet<AccessToFile> _set;

        public FileAccessRepository(FileStorageContext context)
        {
            _context = context;
            _set = context.Set<AccessToFile>();
        }

        public async Task AddAsync(AccessToFile entity)
        {
            await _set.AddAsync(entity);
        }

        public void Delete(AccessToFile entity)
        {
            _set.Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            var entity = await _set.FirstOrDefaultAsync(e => e.Id == id);
            _set.Remove(entity);
        }

        public IQueryable<AccessToFile> FindAll()
        {
            return _set;
        }

        public IQueryable<AccessToFile> FindAllWithDetails()
        {
            return _set.Include(fa => fa.User)
                .Include(fa => fa.File);
        }

        public AccessToFile FindByUserIdAndFileId(string userId, string fileId)
        {
            var fileAccess = _set.FirstOrDefault(fa => fa.UserId == userId && fa.FileId == fileId);

            return fileAccess;
        }

        public async Task<AccessToFile> GetByIdAsync(int id)
        {
            var entity = await _set.FirstOrDefaultAsync(e => e.Id == id);
            return entity;
        }

        public IQueryable<AccessToFile> GetFileAccessByUserId(string userId)
        {
            var filesWithAccess = _set.Include(fa => fa.User)
                .Include(fa => fa.File)
                .Where(fa => fa.UserId == userId);
            return filesWithAccess;
        }

        public void Update(AccessToFile entity)
        {
            _set.Update(entity);           
        }
    }
}
