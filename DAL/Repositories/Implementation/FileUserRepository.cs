﻿using DAL.EF;
using DAL.Entities;
using DAL.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories.Implementation
{
    public class FileUserRepository : IFileUserRepository
    {
        private readonly FileStorageContext _context;
        private readonly DbSet<FileUser> _set;

        public FileUserRepository(FileStorageContext context)
        {
            _context = context;
            _set = context.Set<FileUser>();
        }

        public async Task AddAsync(FileUser entity)
        {
            await _set.AddAsync(entity);
        }

        public void Delete(FileUser entity)
        {
            _set.Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            var entity = await _set.FirstOrDefaultAsync(e => e.Id == id);
            _set.Remove(entity);
        }

        public IQueryable<FileUser> FindAll()
        {
            return _set;
        }

        public IQueryable<FileUser> FindAllWithDetails()
        {
            return _set.Include(fu => fu.User)
                .Include(fu => fu.File);
        }

        public FileUser FindByFileId(string fileId)
        {
            var fileUser = _set.FirstOrDefault(fu => fu.FileId == fileId);

            return fileUser;
        }

        public FileUser FindByUserIdAndFileId(string userId, string fileId)
        {
            var fileUser = _set.FirstOrDefault(fu => fu.UserId == userId && fu.FileId == fileId);

            return fileUser;
        }

        public async Task<FileUser> GetByIdAsync(int id)
        {
            var entity = await _set.FirstOrDefaultAsync(e => e.Id == id);
            return entity;
        }

        public IQueryable<FileUser> GetFilesOfUserById(string id)
        {
            var filesOfUser = _set.Include(fu => fu.User)
                .Include(fu => fu.File)
                .Where(fu => fu.UserId == id);
            return filesOfUser;
        }

        public void Update(FileUser entity)
        {
            _set.Update(entity);           
        }
    }
}
