﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories.Interfaces
{
    public interface IFileUserRepository : IRepository<FileUser>
    {
        IQueryable<FileUser> GetFilesOfUserById(string id);
        IQueryable<FileUser> FindAllWithDetails();
        FileUser FindByUserIdAndFileId(string userId, string fileId);
        FileUser FindByFileId(string fileId);
    }
}
