﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories.Interfaces
{
    public interface IFileRepository
    {
        IQueryable<FilesInfo> FindAll();

        Task<FilesInfo> GetByIdAsync(string id);

        Task AddAsync(FilesInfo entity);

        void Update(FilesInfo entity);

        void Delete(FilesInfo entity);

        Task DeleteByIdAsync(string id);
    }
}
