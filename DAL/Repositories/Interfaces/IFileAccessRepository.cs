﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Repositories.Interfaces
{
    public interface IFileAccessRepository : IRepository<AccessToFile>
    {
        IQueryable<AccessToFile> GetFileAccessByUserId(string userId);
        IQueryable<AccessToFile> FindAllWithDetails();
        AccessToFile FindByUserIdAndFileId(string userId, string fileId);
    }
}
