﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using BLL.Models;
using BLL.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FileStorage.Controllers
{
    /// <summary>
    /// Controller to manipulate files
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class FilesController : ControllerBase
    {
        private readonly IFileService _fileService;

        /// <summary>
        /// Property that returns Id of user, if it's exist
        /// </summary>
        private Guid UserId
        {
            get
            {
                bool isAnyClaims = User.Claims.Any();

                if (!isAnyClaims)
                    return Guid.Empty;
                return Guid.Parse(User.Claims.Single(c => c.Type == ClaimTypes.NameIdentifier).Value);
            }
        }

        /// <summary>
        /// Constructor injects all necessary components
        /// </summary>
        /// <param name="fileService">Service which provides access to files</param>
        public FilesController(IFileService fileService)
        {
            _fileService = fileService;
        }

        /// <summary>
        /// Returns files, which user has access to:
        ///     'Guest' - public files
        ///     'User' - shared and public files
        ///     'Admin' - all files
        /// </summary>
        /// <returns>List of FileUserModel that contains info about file and his owner</returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<List<FileUserModel>>> GetAllFiles()
        {
            List<FileUserModel> files;

            if (UserId == Guid.Empty)
                files = await _fileService.GetFilesAsync();
            else
                files = await _fileService.GetFilesAsync(UserId.ToString());

            return Ok(files);
        }

        /// <summary>
        /// Checks user identity and returns created files
        /// </summary>
        /// <returns>List of FileModel</returns>
        [HttpGet("User")]
        public ActionResult<List<FileModel>> GetUserFiles()
        {
            return Ok(_fileService.GetUserFiles(UserId.ToString()));
        }

        /// <summary>
        /// Checks user identity and returns shared with them files
        /// </summary>
        /// <returns>List of FileUserModel that contains info about file and his owner</returns>
        [HttpGet("Shared")]
        public ActionResult<List<FileUserModel>> GetFilesWithAccess()
        {
            var filesWithAccess = _fileService.GetFilesWithAccess(UserId.ToString());

            return Ok(filesWithAccess);
        }


        /// <summary>
        /// Creates new file 
        /// </summary>
        /// <returns>Ok, if file was successfully created or BadRequest with error message, if cretion was failed</returns>        
        [HttpPost("Create"), DisableRequestSizeLimit]
        public async Task<ActionResult> AddFile()
        {
            try
            {
                var formCollection = await Request.ReadFormAsync();
                var file = formCollection.Files.First();

                if (file.Length == 0)
                    return BadRequest("File length is zero");

                await _fileService.AddAsync(file, UserId.ToString());

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Gives access to file for user, if user owner of file or admin
        /// </summary>
        /// <param name="fileId">Id of file to give access to</param>
        /// <param name="userNameToGiveAccess">Username to give access to</param>
        /// <returns>Ok, if access was successfully given or BadRequest with error message, if operation was failed</returns>
        [HttpPost("Access/Give/{userNameToGiveAccess}")]
        public async Task<ActionResult> GiveAccessToUser([FromBody]string fileId, string userNameToGiveAccess)
        {
            try
            {
                await _fileService.GiveAccessAsync(fileId, userNameToGiveAccess, UserId.ToString());

                return Ok();
            }
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Removes access from file for user, if user owner of file or admin
        /// </summary>
        /// <param name="fileId">Id of file to remove access from</param>
        /// <param name="userNameToRemoveAccess">Username to remove access from</param>
        /// <returns>Ok, if access was successfully removed or BadRequest with error message, if operation was failed</returns>
        [HttpPost("Access/Remove/{userNameToRemoveAccess}")]
        public async Task<ActionResult> RemoveAccessFromUser([FromBody] string fileId, string userNameToRemoveAccess)
        {
            try
            {
                await _fileService.RemoveAccessAsync(fileId, userNameToRemoveAccess, UserId.ToString());

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Changes file privacy, if user owner of file or admin
        /// </summary>
        /// <param name="fileId">Id of file to change privacy</param>
        /// <returns>Ok, if privacy was successfully changed or BadRequest with error message, if operation was failed</returns>
        [HttpPatch("Change/Privacy/{fileId}")]
        public async Task<ActionResult> ChangeFilePrivacy(string fileId)
        {
            try
            {                
                await _fileService.ChangeFilePrivacyAsync(fileId, UserId.ToString());

                return Ok();
            }
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Changes file name, if user owner of file or admin
        /// </summary>
        /// <param name="fileId">Id of file to change name</param>
        /// <returns>Ok, if name was successfully changed or BadRequest with error message, if operation was failed</returns>
        [HttpPatch("Change/Name/{fileId}")]
        public async Task<ActionResult> ChangeFileName([FromBody]string newName, string fileId)
        {
            try
            {               
                await _fileService.ChangeFileNameAsync(fileId, newName, UserId.ToString());

                return Ok();
            }
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Downloads file from Google Drive, if user have access to file
        /// </summary>
        /// <param name="folderPath">Folder to download file to</param>
        /// <param name="fileId">Id of file to download</param>
        /// <returns>File, if file was successfully downloaded or BadRequest with error message, if operation was failed</returns>
        [HttpGet("Download/{fileId}")]
        [AllowAnonymous]
        public async Task<ActionResult> DownloadFile(string fileId)
        {
            try
            {
                DownloadModel download;

                if (UserId == Guid.Empty)
                    download = await _fileService.DownloadFileAsync(fileId);
                else
                    download = await _fileService.DownloadFileAsync(fileId, UserId.ToString());
               
                return File(download.Memory.GetBuffer(), download.ContentType, download.Name);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);                
            }
        }

        /// <summary>
        /// Deletes file from, if user owner of file or admin
        /// </summary>
        /// <param name="fileId">Id of file to delete</param>
        /// <returns>Ok, if file was successfully deleted or BadRequest with error message, if operation was failed</returns>
        [HttpDelete("Delete/{fileId}")]
        public async Task<ActionResult> DeleteFile(string fileId)
        {            
            try
            {                
                await _fileService.DeleteByIdAsync(fileId, UserId.ToString());
                return Ok();
            }
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
