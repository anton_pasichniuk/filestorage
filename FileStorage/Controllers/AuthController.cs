﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using BLL.Models;
using BLL.Services.Interfaces;
using FileStorage.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace FileStorage.Controllers
{
    /// <summary>
    /// Controller to make authentication and registration in FileStorage
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IOptions<JwtOptions> _jwtOptions;
        private readonly IUserService _userService;

        /// <summary>
        /// Constructor injects all necessary components
        /// </summary>
        /// <param name="jwtOptions">Options from appsettings.json to set JWT parameters</param>
        /// <param name="userService">Service which provides access to user</param>
        public AuthController(IOptions<JwtOptions> jwtOptions,
            IUserService userService)
        {
            _jwtOptions = jwtOptions;
            _userService = userService;
        }

        /// <summary>
        /// Post method to check user to authenticate and returns generated JWT
        /// </summary>
        /// <param name="request">LoginModel that contains user Email and Password</param>
        /// <returns>Ok with JWT, if authentication was successfully done or Unauthorize, if not</returns>
        [HttpPost("Login")]
        public async Task<ActionResult> Login([FromBody]LoginModel request)
        {            
            var user = await _userService.FindByEmailAndPasswordAsync(request.Email, request.Password);

            if(user != null)
            {
                var token = await GenerateJwt(user);

                var userRoles = await _userService.GetRolesAsync(user.Id);

                return Ok(new
                {
                    accessToken = token,
                    userId = user.Id,
                    username = user.UserName,
                    userRoles = userRoles
                });
            }

            return Unauthorized();
        }

        /// <summary>
        /// Generates JWT according to user info
        /// </summary>
        /// <param name="user">Info about user</param>
        /// <returns>JWT, which contains user Email, Id and Roles</returns>
        private async Task<string> GenerateJwt(UserModel user)
        {
            var jwtParams = _jwtOptions.Value;

            var securityKey = jwtParams.GetSymmetricSecurityKey();
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new List<Claim>()
            {
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim(JwtRegisteredClaimNames.Sub, user.Id)
            };

            foreach (var role in await _userService.GetRolesAsync(user.Id))
                claims.Add(new Claim("role", role));

            var token = new JwtSecurityToken(
                issuer: jwtParams.Issuer,
                audience: jwtParams.Audience,
                claims: claims,
                notBefore: DateTime.UtcNow,
                expires: DateTime.UtcNow.AddMinutes(jwtParams.LifeTime),
                signingCredentials: credentials);

            var a = token.ValidFrom; var b = token.ValidTo;

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        /// <summary>
        /// Checks user input according to data annotation in UserModel and UserManager(Identity) check
        /// </summary>
        /// <param name="newUser">New user info</param>
        /// <returns>Ok, if user was successfully registered or BadRequest with error message, if registration was failed</returns>
        [HttpPost("Register")]
        public async Task<ActionResult> Register([FromBody]UserModel newUser)
        {
            try
            {
                await _userService.AddAsync(newUser);

                return Ok();
            }          
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
