﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using BLL.Models;
using BLL.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FileStorage.Controllers
{
    /// <summary>
    /// Controller to manipulate users
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;

        /// <summary>
        /// Property that returns Id of user, if it's exist
        /// </summary>
        private Guid UserId
        {
            get
            {
                bool isAnyClaims = User.Claims.Any();

                if (!isAnyClaims)
                    return Guid.Empty;
                return Guid.Parse(User.Claims.Single(c => c.Type == ClaimTypes.NameIdentifier).Value);
            }
        }

        /// <summary>
        /// Constructor injects all necessary components
        /// </summary>
        /// <param name="userService">Service which provides access to user</param>
        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// Provides access to users info for 'Admin'
        /// </summary>
        /// <returns>List of UserModel that contains info about user</returns>
        [HttpGet]
        [Authorize(Roles = "Admin")]
        public ActionResult<List<UserModel>> GetAll()
        {
            var users = _userService.GetAll();

            return Ok(users);
        }

        /// <summary>
        /// Returns personal info of user by their Id
        /// </summary>
        /// <returns>UserModel that contains info about user</returns>
        [HttpGet("PersonalInfo")]
        public async Task<ActionResult<UserModel>> GetUserInfo()
        {
            var user = await _userService.GetByIdAsync(UserId.ToString());

            return Ok(user);
        }

        /// <summary>
        /// Returns info about user by their Id for 'Admin' 
        /// </summary>
        /// <param name="userId">Id of user</param>
        /// <returns>UserModel that contains info about user</returns>
        [HttpGet("{userId}")]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult<UserModel>> GetUserInfo(string userId)
        {
            var user = await _userService.GetByIdAsync(userId);

            return Ok(user);
        }

        /// <summary>
        /// Changes personal user info by user Id
        /// </summary>
        /// <param name="newInfo">New info of user</param>
        /// <returns>Ok, if user info was successfully changed or BadRequest with error message, if operation was failed</returns>
        [HttpPatch("Change")]
        public async Task<ActionResult> UpdateUserInfo([FromBody]UpdateUserModel newInfo)
        {
            try
            {
                await _userService.UpdateAsync(UserId.ToString(), newInfo);

                return Ok();
            }
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Changes user info by user Id for 'Admin'
        /// </summary>
        /// <param name="newInfo">New info of user</param>
        /// <returns>Ok, if user info was successfully changed or BadRequest with error message, if operation was failed</returns>
        [HttpPatch("Change/{userId}")]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> UpdateUserInfo([FromBody] UpdateUserModel newInfo, string userId)
        {
            try
            {
                await _userService.UpdateAsync(userId, newInfo);

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Removes user from Db and Google Drive by their Id for 'Admin'
        /// </summary>
        /// <param name="userId">Id of user</param>
        /// <returns>Ok, if user was successfully removed or BadRequest with error message, if operation was failed</returns>
        [HttpDelete("Delete")]
        public async Task<ActionResult> DetereUser()
        {
            try
            {
                await _userService.DeleteByIdAsync(UserId.ToString());

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Removes user from Db and Google Drive by their Id for 'Admin'
        /// </summary>
        /// <param name="userId">Id of user</param>
        /// <returns>Ok, if user was successfully removed or BadRequest with error message, if operation was failed</returns>
        [HttpDelete("Delete/{userId}")]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> DeleteUser(string userId)
        {
            try
            {
                await _userService.DeleteByIdAsync(userId);

                return Ok();
            }
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
